LearnBatch

This guide, created by Gabriel Ashcraft (creator of UniverseRPG), will teach you everything about the old programming language called Batch. It even has exercises for you to do to help you learn it.

Start with CHAPTER1.txt and work your way up through each chapter, all the way to END.txt. I hope you enjoy this guide!

-Gabriel Ashcraft (sonicdude5787 on BitBucket)