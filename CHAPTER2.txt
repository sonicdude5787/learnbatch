CHAPTER 2 - The Basics

Let's get into the basics of Batch, shall we?
With every block of code in Batch, there is a label. A label is a block of code that functions can refer to at any point in the Batch file. For example:

:epiclabel

That wouldn't do anything for your program, but it is very necessary in Batch. Use it to separate parts of code. Also note that there can NOT be in any spaces in labels.
To make it easy, think of levels in a video game. They could be like labels. For example, there could be a label that says :level1 and then :level2 and so on.

First, let's learn about the CLS function. All it does is clear the text out of the Command Prompt the Batch file opens automatically. For example:

:epiclabel
cls

That line of code would clear the screen. But, there is nothing showing on the screen if you just put that command alone in a Batch file.

Next, let's learn about the ECHO function. It is very much like a PRINT command that you might find on an old PC such as the Apple II. It simply "echoes" a line of text to the screen. For example:

:epiclabel
cls
echo Hello World!

That portion of code would show the words "Hello World!" on the Command Prompt the Batch file opens automatically, but it would clear the screen before it shows the words. But, it wouldn't be there for long, as the Command Prompt would instantly close. Why? Because you didn't add a stopping point to the file.

This is where the PAUSE and EXIT functions come in handy. The PAUSE function does what it says.. it pauses anything from going through the Command Prompt until someone pushes a key on their keyboard. Then, it will continue loading code. The EXIT function does what it says.. it closes the Command Prompt. For example:

:epiclabel
cls
echo Hello World!
pause
exit

That portion of code would clear the screen. Then, it would show the words "Hello World!" on the Command Prompt. Then, it would pause any code from loading until the user pushes a key on their keyboard. Then, it will continue loading code. So, it would then exit the Command Prompt after the user pushes a key on their keyboard.

EXERCISE 1 - My Favorite Programming Language Is..

Create a Batch file that says "My favorite programming language is.." and put the name of your favorite programming language after the word "is". Remember to clear the screen before it echoes, and make sure to put a label before the main piece of code. Once you are finished, scroll down farther into this text document to see the answer.




































EXERCISE 1 ANSWER

:epiclabel (Could have been named whatever you want, as long as it has no spaces)
cls
echo My favorite programming language is Batch. (The programming language could have been whatever you want, as long as it is an existing programming language)
pause
exit